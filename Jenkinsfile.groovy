pipeline {
    agent any
    stages {
        stage('SCM Checkout') {
            steps {
                git url: "https://gitlab.com/SupriyaReddyB/testing_lk.git", branch: 'main'
            }
        }
        stage('Archive Artifacts') {
            steps {
                archiveArtifacts '**/*.html'
            }
        }
        stage('Deployment') {
            steps {
                script {
                    sshPublisher(
                        publishers: [sshPublisherDesc(configName: 'webserver')],
                        transfers: [sshTransfer(
                            exclude: '',
                            execCommand: '',
                            execTimeout: 120000,
                            flatten: true,
                            makeEmptyDirs: false,
                            noDefaultExclude: false,
                            patternSeparator: '[,]+',
                            remoteDirectory: '',
                            remoteDirectorySDF: false,
                            removePrefix: '',
                            sourceFiles: '**/*.html'
                        )]
                    )
                }
            }
        }
    }
}
